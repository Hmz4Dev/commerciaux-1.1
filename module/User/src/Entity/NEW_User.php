<?php
namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a registered NEW_User.
 * @ORM\Entity()
 * @ORM\Table(name="NEW_User")
 */
class NEW_User 
{
    // User Profil constants.
    const Profil_Commer       = 1; 
    const Profil_user      = 2; 

    /**
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(name="email")
     */
    protected $email;
    /**
     * @ORM\Column(name="Prenom")
     */
    protected $Prenom;
    /**
     * @ORM\Column(name="Nom")
     */
    protected $Nom;
    /**
     * @ORM\Column(name="Login")
     */
    protected $Login;
    /**
     * @ORM\Column(name="Mdp")
     */
    protected $Mdp;
    /**
     * @ORM\Column(name="Tel")
     */
    protected $Tel;

    /**
     * @ORM\Column(name="Profil")
     */
    protected $Profil;
    /**
     * @ORM\Column(name="Assistante")
     */
    protected $Assistante;
    /**
     * @ORM\Column(name="Zonegeo")
     */
    protected $Zonegeo;
    /**
     * @ORM\Column(name="DateDernierLogin")
     */
    protected $DateDernierLogin;
    /**
     * @ORM\Column(name="NombreDeLogins")
     */
    protected $NombreDeLogins;
    /**
     * @ORM\Column(name="CreeLe")
     */
protected $CreeLe;
/**
		* @ORM\Column(name="CreePar")
		*/
protected $CreePar;
/**
		* @ORM\Column(name="ModifieLe")
		*/
protected $ModifieLe;
/**
		* @ORM\Column(name="ModifiePar")
		*/

protected $ModifiePar;
/**
		* @ORM\Column(name="Corbeille")
		*/

protected $Corbeille;



    /**
     * @ORM\ManyToMany(targetEntity="User\Entity\Role")
     * @ORM\JoinTable(name="user_role",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     */
    private $roles;
    
    /**
     * Constructor.
     */
     
    
     public function __construct()
     {
       $this->roles = new ArrayCollection();
     }
     
     
    /**
     * Returns user ID.
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets user ID.
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * Returns email.
     * @return string
     */
    public function getemail()
    {
        return $this->email;
    }

    /**
     * Sets email.
     * @param string $email
     */
    public function setemail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns full name.
     * @return string
     */
    public function getNom()
    {
        return $this->Nom;
    }

    /**
     * Sets full name.
     * @param string $Nom
     */
    public function setNom($Nom)
    {
        $this->Nom = $Nom;
    }

    /**
     * Returns Prenom.
     * @return string
     */
    public function getPrenom()
    {
        return $this->Prenom;
    }

    /**
     * Sets full Prenom.
     * @param string $Prenom
     */
    public function setPrenom($Prenom)
    {
        $this->Prenom = $Prenom;
    }
    /**
     * Returns Profil.
     * @return int
     */
    public function getProfil()
    {
        return $this->Profil;
    }

    /**
     * Returns possible Profiles as array.
     * @return array
     */
    public static function getProfilList()
    {
        return [
            self::Profil_Commer => 'Commer',
            self::Profil_user => 'user'
        ];
    }

    /**
     * Returns user Profil as string.
     * @return string
     */
    public function getProfilAsString()
    {
        $list = self::getProfilList();
        if (isset($list[$this->Profil]))
            return $list[$this->Profil];

        return 'Unknown';
    }

    /**
     * Sets Profil.
     * @param int $Profil
     */
    public function setProfil($Profil)
    {
        $this->Profil = $Profil;
    }
    /**
     * Returns Login.
     * @return string
     */
    public function getLogin()
    {
       return $this->Login;
    }

    /**
     * Sets Login.
     * @param string $Login
     */
    public function setLogin($Login)
    {
        $this->Login = $Login;
    }
    /**
     * Returns Mdp.
     * @return string
     */
    public function getMdp()
    {
       return $this->Mdp;
    }

    /**
     * Sets Mdp.
     * @param string $Mdp
     */
    public function setMdp($Mdp)
    {
        $this->Mdp = $Mdp;
    }

  /**
   * Returns Assistante.
   * @return int
   */
  public function getAssistante()
  {
     return $this->Assistante;
  }

  /**
   * Sets Assistante.
   * @param int $Assistante
   */
  public function setAssistante($Assistante)
  {
      $this->Assistante = $Assistante;
  }
  /**
   * Returns Zonegeo.
   * @return int
   */
  public function getZonegeo()
  {
     return $this->Zonegeo;
  }

  /**
   * Sets Zonegeo.
   * @param int $Zonegeo
   */
  public function setZonegeo($Zonegeo)
  {
      $this->Zonegeo = $Zonegeo;
  }
  /**
   * Returns the date of user login.
   * @return int
   */
  public function getDateDerierLogin()
  {
      return $this->DateDerierLogin;
  }

  /**
   * Sets the date when this user was login.
   * @param string $DateDerierLogin
   */
  public function setDateDerierLogin($DateDerierLogin)
  {
      $this->DateDerierLogin = $DateDerierLogin;
  }
  /**
   * Returns the Nombre of user login.
   * @return int
   */
  public function getNombreDeLogins()
  {
      return $this->NombreDeLogins;
  }

  /**
   * Sets the login when this user was login.
   * @param string $NombreDeLogins
   */
  public function setNombreDeLogins($NombreDeLogins)
  {
      $this->NombreDeLogins = $NombreDeLogins;
  }
    /**
     * Returns the date of user creation.
     * @return int
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Sets the date when this user was created.
     * @param string $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * Returns password reset token.
     * @return string
     */
    public function getResetPasswordToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * Sets password reset token.
     * @param string $token
     */
    public function setPasswordResetToken($token)
    {
        $this->passwordResetToken = $token;
    }

    /**
     * Returns password reset token's creation date.
     * @return string
     */
    public function getPasswordResetTokenCreationDate()
    {
        return $this->passwordResetTokenCreationDate;
    }

    /**
     * Sets password reset token's creation date.
     * @param string $date
     */
    public function setPasswordResetTokenCreationDate($date)
    {
        $this->passwordResetTokenCreationDate = $date;
    }
	//--------------------------------------------------------------
	/**
     * Sets CreeLe of this post.
     * @param string $CreeLe
     */
	public function setCreeLe($CreeLe)
	{

			$this->CreeLe =$CreeLe ;


	}
	//--------------------------------------------------------------
	/**
     * Returns CreeLe of this post.
     * @return string
     */
	public function getCreeLe()
	{
		if(!isset($this->CreeLe)) return null;
		return $this->CreeLe;
	}
  	//--------------------------------------------------------------
	public function setCreePar($CreePar)
	{
		$this->_CreePar = $CreePar;
		return $this;
	}
	//--------------------------------------------------------------
	public function getCreePar()
	{
		return $this->_CreePar;
	}
	//--------------------------------------------------------------
	public function setModifieLe($ModifieLe)
	{
		if($ModifieLe!='' && $ModifieLe!=null && $ModifieLe!='0000-00-00 00:00:00')
			$this->_ModifieLe = new DateTime($ModifieLe) ;
		else $this->_ModifieLe = null;
		return $this;
	}
	//--------------------------------------------------------------
	public function getModifieLe($format='Y-m-d H:i:s')
	{
		if(!isset($this->_ModifieLe)) return null;
		return $this->_ModifieLe->format($format);
	}
	//--------------------------------------------------------------
	public function setModifiePar($ModifiePar)
	{
		$this->_ModifiePar = $ModifiePar;
		return $this;
	}
	//--------------------------------------------------------------
	public function getModifiePar()
	{
		return $this->_ModifiePar;
	}
	//----------------------------------------------------------------
	//--------------------------------------------------------------
	public function setCorbeille($Corbeille)
	{
		$this->_Corbeille = $Corbeille;
		return $this;
	}
	//--------------------------------------------------------------
	public function getCorbeille()
	{
		return $this->_Corbeille;
	}
	//----------------------------------------------------------------


    /**
     * Returns the array of roles assigned to this user.
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Returns the string of assigned role names.
     */
    public function getRolesAsString()
    {
        $roleList = '';

        $count = count($this->roles);
        $i = 0;
        foreach ($this->roles as $role) {
            $roleList .= $role->getName();
            if ($i<$count-1)
                $roleList .= ', ';
            $i++;
        }

        return $roleList;
    }

    /**
     * Assigns a role to user.
     */
    public function addRole($role)
    {
        $this->roles->add($role);
    }
}

