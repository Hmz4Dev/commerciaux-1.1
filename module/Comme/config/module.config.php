<?php
namespace Comme;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'router' => [
        'routes' => [

            'comme' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/commerciaux[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller'    => Controller\CommeController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\CommeController::class => Controller\Factory\CommeControllerFactory::class,
        ],
    ],
   
    'access_filter' => [
        'controllers' => [
            Controller\CommeController::class => [
                ['actions' => ['index', 'searchresult','fichclient'], 'allow' => '+Commer.view']
            ],
        ]
    ],
    'service_manager' => [
        'factories' => [
          Service\CommeManager::class => Service\Factory\CommeManagerFactory::class,
            Service\RbacAssertionManager::class => Service\Factory\RbacAssertionManagerFactory::class,
            Service\paysManager::class  => Service\Factory\paysManagerFactory::class,
            Service\contactsManager::class  => Service\Factory\contactsManagerFactory::class,
            Service\visiteManager::class  => Service\Factory\visiteManagerFactory::class,
            Service\prospectsManager::class  => Service\Factory\prospectsManagerFactory::class,

            ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'aliases' => [
            'access' => View\Helper\Access::class,
            'currentUser' => View\Helper\CurrentUser::class,
        ],

          'strategies'          =>[
            'ViewJsonStrategy',
          ],
    ],

    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],
];
