<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
/**
 * This class represents a registered Designation.
 * @ORM\Entity()
 * @ORM\Table(name="NEW_FamilleClients")
 */

class NEW_FamilleClients extends Abstract_Model
{

	/**
	* @ORM\Column(name="fid")
	*/
	protected $fid;


     /**
     * @ORM\Column(name="Designation")
     */
     protected $Designation;
     public function __construct(array $options = null)
         {

          $this->_rec_name = 'nom';
        parent::__construct($options);
        }

    /**
     * Returns fid
     * @return string
     */

	 public function getfid(){ return $this->fid; }
	 /**
     * Sets fid.
     * @param string $fid
     */
   	 public function setfid($code){$this->fid=$code; return $this; }

   	  /**
     * Returns Designation.
     * @return string
     */

   	 public function getDesignation(){return $this->Designation; }
   	 /**
     * Sets Designation.
     * @param string $Designation
     */
	 public function setDesignation($Designation){$this->Designation=$Designation; return $this; }




}
