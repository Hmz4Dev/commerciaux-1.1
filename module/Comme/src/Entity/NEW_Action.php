<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a registered NEW_Action.
 * @ORM\Entity()
 * @ORM\Table(name="NEW_Action")
 */

class NEW_Action extends Abstract_Model
{
	/**
 * @ORM\Column(name="Designation")
 */
      public $Designation;

      /**
     * @ORM\Column(name="Valeur")
     */

      public $Valeur;

      /**
     * @ORM\Column(name="Type")
     */
     public $Type;

     public function __construct(array $options = null)
         {

          $this->_rec_name = 'nom';
        parent::__construct($options);
        }


   	  /**
     * Returns Designation.
     * @return string
     */
   	 public function getDesignation(){return $this->Designation; }
   	 /**
     * Sets Designation.
     * @param string $Designation
     */
	 public function setDesignation($Designation){$this->Designation=$Designation; return $this; }

   /**
  * Returns Valeur.
  * @return string
  */
   public function getValeur(){return $this->Valeur; }
   /**
  * Sets Valeur.
  * @param string $Valeur
  */
public function setValeur($Valeur){$this->Valeur=$Valeur; return $this; }

/**
* Returns Type.
* @return string
*/
public function getType(){return $this->Type; }
/**
* Sets Type.
* @param string $Type
*/
public function setType($Type){$this->Type=$Type; return $this; }

}
