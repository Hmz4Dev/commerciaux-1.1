<?php
namespace Comme\Entity;

use Comme\Entity\Abstract_Model;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This class represents a single post in a blog.
 * @ORM\Entity(repositoryClass="\Comme\Repository\NEW_PaysRepository")
 * @ORM\Table(name="NEW_Pays")
 */

class NEW_Pays extends Abstract_Model
{
	/**
 * @ORM\Column(name="Nom")
 */
     public $Nom;

     public function __construct(array $options = null)
         {

          $this->_rec_name = 'nom';
        parent::__construct($options);
        }


   	  /**
     * Returns Nom.
     * @return string
     */
   	 public function getNom(){return $this->Nom; }
   	 /**
     * Sets Nom.
     * @param string $Nom
     */
	 public function setfonction($fonction){$this->Nom=$Nom; return $this; }

}
