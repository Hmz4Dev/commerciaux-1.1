<?php

namespace Comme\Form;

use Zend\Form\Form;
use Zend\Form\fieldset;
use Zend\Form\Element;
use Zend\InputFilter\InputFilter;
use Comme\Entity\NEW_Action;
use Comme\Entity\NEW_Pays;
use Comme\Entity\NEW_FamilleClients;
use Comme\Entity\NEW_ZoneGeographique;
use Comme\Entity\contacts;
//use Comme\Validator\SocitExistsValidator;
//use  Comme\Validator\ChampValidator;


class NewActionForm extends Form {


  	    private $entityManager=null;
  	    private $prospects=null;


public function __construct($entityManager=null,$prospects=null)
{

    parent::__construct('newactionform');
    $this->setAttribute('method', 'post');

    $this->entityManager = $entityManager;
    $this->prospects = $prospects;
    $this->addElements();
  //  $this->addInputFilter();

}
 public function addElements(){

    $this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
         ));

 $this->add([
               'type' => 'Select',
               'name' => 'idAction',
               'options' => [
              // 'label'   => '',
               'value_options' =>$this->getaction(),
	                        ],
               'attributes' =>[
                                 'value' => '0', //set selected to '1'
                                 'style' => 'width: 100%;',
                                  'placeholder'=>true,
'class'=>'form-control',
                                ],

                               ]);

     $this->add([
               'type' => 'Select',
               'name' => 'idContact1',
               'options' => [
            //   'label'   => '',
               'value_options' =>$this->getresponsable(),
                          ],
               'attributes' =>[
                                 'value' => '0', //set selected to '1'
                                 'style' => 'width: 100%;',
                                  'placeholder'=>'Champs obligatoire',
                                  'class'=>'form-control',
                                ],

                               ]);


     $this->add([
               'type' => 'Select',
               'name' => 'idContact2',
               'options' => [
              // 'label'   => '',
               'value_options' =>$this->getresponsable(),
                          ],
               'attributes' =>[
                                 'value' => '0', //set selected to '1'
                                 'style' => 'width: 100%;',
                               //   'placeholder'=>'Champs obligatoire'
'class'=>'form-control',

                                ],

                               ]);

    $this->add([
               'type' => 'Select',
               'name' => 'idContact3',
               'options' => [
            //   'label'   => '',
               'value_options' =>$this->getresponsable(),
                          ],
               'attributes' =>[
                                 'value' => '0', //set selected to '1'
                                 'style' => 'width: 100%;',
                                 'placeholder'=>'Champs obligatoire',
'class'=>'form-control',

                                ],

                               ]);


                               $this->add(array(
                                         'name'=>'Commentaire',
                                   'attributes'=>array(
                                                'type'=>'textarea',
                                                        'style'=>'width:100%',
                                                        'placeholder'=>'Commentaire ...',
                                                        'class'=>'form-control',

                                                          ),
                                         'options'=>array(
                                                // 'label'=>'Ville'
                                                         )
                                          ));

                                $this->add(array(
        'type' => 'Zend\Form\Element\MultiCheckbox',
        'name' => 'Destinataires',
        'options' => array(
            'label' => 'What do you like ?',
            'value_options' => array(
                '1' => 'Direction',
                '2' => 'Bureau d étude',
                '3' => 'Expedition',
                '4' => 'Production ',
                '5' => 'Qualité '
            ),
        )
    ));


				   $this->add(array(
				   'name'=>'Annuler',
				   'type'=>'submit',
				   'attributes'=>array(
				        'value'=>'Annuler',
				        'class'=>'bbtn btn-default btn-sm',
				        'size'=>30,
				        'id' => 'submitbutton'

						)

			     	   ));
				    $this->add(array(
				   'name'=>'enregistrerv',
				   'type'=>'submit',
				   'attributes'=>array(
				        'value'=>'Enredistrer',
				        'class'=>'btn btn-primary btn-sm',
						'id' => 'submitbutton'
						)

				   ));
                    }


public function getaction()
{

  $famille=$this->entityManager->getRepository(NEW_Action::class)->findAll();
  $selectData = array();
        $selectData[0] ="---------- Action ------------";
        foreach ($famille as $res) {
            $selectData[$res->getid()] = $res->getDesignation();
        }
        return $selectData;
}

public function getresponsable()
{

  $pays=$this->entityManager->getRepository(contacts::class)->findAll();
  $selectData = array();
        $selectData[0] ="---------- Responsable --------";
        foreach ($pays as $res) {
            $selectData[$res->getid()] = $res->getnom();
        }
        return $selectData;
}


}
