<?php
namespace Comme\Repository;

use Doctrine\ORM\EntityRepository;
use Comme\Entity\prospects;

/**
 * This is the custom repository class for prospects entity.
 */
class prospectsRepository extends EntityRepository
{


    /**
     * Finds all published posts having the given tag.
     * @param string $tagName Name of the tag.
     * @return array
     */

    public function findprospects($tagName)
    {

        if (isset($tagName) && ctype_digit($tagName)) {

            $indsearch='pr.id = ?2';
            $fieldquoted='pr.id';
        }
        elseif (is_string ($tagName)) {
        $indsearch='pr.nom = ?2';
         $fieldquoted='pr.nom';
        }
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('pr')
            ->from(prospects::class, 'pr')
            ->Where($indsearch)
            ->orderBy('pr.id', 'DESC')
            ->setParameter('2', $tagName);
        $prospects = $queryBuilder->getQuery()->getResult();

        if($prospects==null)
        {
             $len=$txlen=strlen($tagName);
             $comp=0;
             $rest = substr($tagName,0,-$comp+1);
            while($prospects==null && $txlen>=1)
            {   
               
               
                  $entityManager = $this->getEntityManager();
                  $queryBuilder  = $entityManager->createQueryBuilder();
                  $queryBuilder  ->select('pr')
                                 ->from(prospects::class, 'pr')
                                 ->Where('SUBSTRING(pr.nom,1,?1) = ?2') 
                                 ->orderBy('pr.id', 'DESC')
                                 ->setParameter('1', $txlen)
                                 ->setParameter('2', $rest);
          
                 $prospects = $queryBuilder->getQuery()->getResult();

                 $txlen--;
                 $comp++;
                 $rest = substr($tagName,0,-$comp);
            }
        }
        return $prospects;
    }


    /**
     * Finds all published posts having the given tag.
     * @param string $tagName Name of the tag.
     * @return array
     */

    public function findprospectsID($tagName)
    {

        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('pr')
            ->from(prospects::class, 'pr')
            ->Where('pr.id = ?2')
            ->orderBy('pr.id', 'DESC')
            ->setParameter('2', $tagName);
        $prospects = $queryBuilder->getQuery()->getResult();
        return $prospects;
    }

}
