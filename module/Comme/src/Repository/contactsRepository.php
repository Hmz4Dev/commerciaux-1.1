<?php
namespace Comme\Repository;

use Doctrine\ORM\EntityRepository;
use Comme\Entity\contacts;

/**
 * This is the custom repository class for contacts entity.
 */
class contactsRepository extends EntityRepository
{

    /**
     * Finds all published posts having the given tag.
     * @param integer $idProspect Name of the tag.
     * @return array
     */

    public function getAllcontactsEntreprise($idProspect)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('C')
            ->from(contacts::class, 'C')
            ->Where('C.idProspect = ?2')
            ->orderBy('C.id', 'DESC')
            ->setParameter('2', $idProspect);
        $contactsResult = $queryBuilder->getQuery()->getResult();
        return $contactsResult;
    }


}
