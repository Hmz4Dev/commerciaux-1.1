<?php
namespace Comme\Service;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Comme\Entity\Abstract_Model;
use Comme\Entity\contacts;
use Comme\Entity\NEW_FonctionContacts;
use Zend\Math\Rand;

class contactsManager
{
	 /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */

    public $entityManager;



    public function __construct($entityManager)
    {

        $this->entityManager = $entityManager;

    }

     public function getfonctionAsString($ind)
    {
        $fonction=$this->entityManager->getRepository(NEW_FonctionContacts::class)->find($ind);

        return $fonction->getDesignation();
    }
    /**
     * Finds all published posts having the given tag.
     * @param integer $tagName Name of the tag.
     * @return array
     */

    public function findcontacts($tagName)
    {
			$contacts = $this->entityManager->getRepository(contacts::class)
                    ->getAllcontactsEntreprise($tagName);
										return $contacts;
    }
     public function save($data,$idprospect)
       {
		$contacts=new contacts();
        $contacts->setOptions($data);
     	$id = $contacts->getid();
     		try{
     		    $datenow=date('Y-m-d H:i:s');
		   
			if (empty($id) || $id=='0') {
                $contacts->setOptions($data);
				$contacts->setCreeLe($datenow);
                $contacts->setidProspect($idprospect);
              //  if($contacts->getcp()=='') $contacts->setcp(00000);   
			//	$data['CreePar']=Zend_Auth::getInstance()->getIdentity()->Login;

				
			//	$model->setid($id);
				
					  // Add the entity to the entity manager.
                  $this->entityManager->persist($contacts);
        
                     // Apply changes to database.
                  $this->entityManager->flush();
        
        return $contacts;
			}
	
		}catch(Exception $e){
		    
			return false;
		}
		
       
    }

}
