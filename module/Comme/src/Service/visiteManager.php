<?php
namespace Comme\Service;

use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Comme\Entity\Abstract_Model;
use Comme\Entity\NEW_Visite;
use Comme\Entity\NEW_Action;
use Zend\Math\Rand;

class visiteManager
{
	 /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */

    public $entityManager;



    public function __construct($entityManager)
    {

        $this->entityManager = $entityManager;

    }

		public function getactionAsString($ind)
	 {
			 $action=$this->entityManager->getRepository(NEW_Action::class)->find($ind);

			 return $action->getDesignation();
	 }
    /**
     * Finds all published posts having the given tag.
     * @param integer $tagName Name of the tag.
     * @return array
     */

    public function findvisites($tagName)
    {
			$visites = $this->entityManager->getRepository(NEW_Visite::class)
                    ->getAllvisitesEntreprise($tagName);
										return $visites;
    }
    public function design($iddesgn)
    { 
          
        if($iddesgn==1) $designation='Direction';
        if($iddesgn==2) $designation='Bureau d étude';
        if($iddesgn==3) $designation='Expedition';
        if($iddesgn==4) $designation='Production';
        if($iddesgn==5) $designation='Qualité';
        
        
    }

    public function save($data,$idprospect)
       {
		$visites=new NEW_Visite();
        $visites->setOptions($data);
        //$Destinataires=$this->design($data['Destinataires']);

     	$id = $visites->getid();
     		try{
     		    $datenow=date('Y-m-d H:i:s');
		   
			if (empty($id) || $id=='0') {
                $visites->setOptions($data);
				$visites->setCreeLe($datenow);
                $visites->setidProspect($idprospect);
             //   $visites->setDestinataires($Destinataires);
              //  if($contacts->getcp()=='') $contacts->setcp(00000);   
			//	$data['CreePar']=Zend_Auth::getInstance()->getIdentity()->Login;

				
			//	$model->setid($id);
				
					  // Add the entity to the entity manager.
                  $this->entityManager->persist($visites);
        
                     // Apply changes to database.
                  $this->entityManager->flush();
        
        return $visites;
			}
	
		}catch(Exception $e){
		    
			return false;
		}
		
       
    }


}
