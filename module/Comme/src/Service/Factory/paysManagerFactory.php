<?php
namespace Comme\Service\Factory;

use Interop\Container\ContainerInterface;
use Comme\Service\paysManager;
use Zend\ServiceManager\Factory\FactoryInterface;


class paysManagerFactory implements FactoryInterface
{

	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

		return new paysManager($entityManager);
	}
}
