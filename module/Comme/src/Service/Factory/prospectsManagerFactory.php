<?php
namespace Comme\Service\Factory;

use Interop\Container\ContainerInterface;
use Comme\Service\prospectsManager;
use Zend\ServiceManager\Factory\FactoryInterface;


class prospectsManagerFactory implements FactoryInterface
{

	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

		return new prospectsManager($entityManager);
	}
}
